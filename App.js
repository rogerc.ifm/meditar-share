import React from 'react';
import { Text, View } from 'react-native';
import { TabNavigator, StackNavigator, TabBarBottom  } from 'react-navigation';
import { Meditacoes } from './screens/Meditacoes';
import { Meditacao } from './screens/meditacao';
import { Mensagens } from './screens/Mensagens';
import { Mensagem } from './screens/Mensagem';
import { SettingsScreen } from './screens/SettingsScreen';
import { NewUser } from './screens/NewUser';
import { Cursos } from './screens/Cursos';
import { Curso } from './screens/Curso';
import { templateStyle } from './screens/templateStyle';
import { AsyncStorage } from 'react-native';
import { Audio } from 'expo';
import { AdminHome } from './screens/AdminHome';
import { GenericPush } from './screens/GenericPush';
import { FreeMessage } from './screens/FreeMessage';
import { PlenoMessage } from './screens/PlenoMessage';
import { PostFree } from './screens/PostFree';
import { PostPleno } from './screens/PostPleno';
import Octicons from 'react-native-vector-icons/Octicons';
import Home from './screens/Home';
import Expo from 'expo';
import Ionicons from 'react-native-vector-icons/Ionicons';

Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

export const FeedStack = StackNavigator(
  {
    Home: {
      screen: Home,
    },
    AdminHome: {
      screen: AdminHome
    },
    GenericPush: {
      screen: GenericPush
    },
    FreeMessage: {
      screen: FreeMessage
    },
    PlenoMessage: {
      screen: PlenoMessage
    },
    PostFree: {
      screen: PostFree
    },
    PostPleno: {
      screen: PostPleno
    },
  },
  {
    navigationOptions: {
      headerTintColor: templateStyle.meditarColor,
    },
  }
);

export const VideoStack = StackNavigator(
  {
    Meditacoes: {
      screen: Meditacoes
    },
    Meditacao: {
      screen: Meditacao
    }
  },
  {
    navigationOptions: {
      headerTintColor: templateStyle.meditarColor,
    },
  }
);


export const MensagemStack = StackNavigator(
  {
    Mensagens: {
      screen: Mensagens
    },
    Mensagem: {
      screen: Mensagem
    }
  },
  {
    navigationOptions: {
      headerTintColor: templateStyle.meditarColor,
    },
  }
);


export const CursosStack = StackNavigator(
  {
    Cursos: {
      screen: Cursos
    },
    Curso: {
      screen: Curso
    }
  },
  {
    navigationOptions: {
      headerTintColor: templateStyle.meditarColor,
    },
  }
);


export const PerfilStack = StackNavigator(
  {
    Perfil: {
      screen: SettingsScreen
    },
    
    NewUser: {
      screen: NewUser
    }
  },
  {
    navigationOptions: {
      headerTintColor: templateStyle.meditarColor,
    },
  }
);

export default TabNavigator(
  {
    Home:       { screen: FeedStack },
    Meditações: { screen: VideoStack },
    //Cursos:     { screen: CursosStack },
    Mensagens:   { screen: MensagemStack },
    Perfil:     { screen: PerfilStack }
  },

  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-videocam${focused ? '' : '-outline'}`;
        } 
        else if (routeName === 'Perfil') {
          iconName = `ios-person${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Meditações') {
          iconName = `ios-time${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Mensagens') {
          iconName = `ios-text${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Cursos') {
          iconName = `mortar-board${focused ? '' : ''}`;
          return <Octicons name={iconName} size={25} color={tintColor} />;
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: templateStyle.meditarColor,
      inactiveTintColor: 'gray',
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    lazy: true
  }
);
