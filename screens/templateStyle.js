// Teste de Cores
//const meditarColor = '#95659f';
const meditarColor = '#6a56b1';


export const templateStyle = {
	
	meditarColor: meditarColor,

	screenContainer: {
	  flex: 1,
	  backgroundColor: '#fff'
	},

	screenContainerNoHeader: {
	  flex: 1,
	  backgroundColor: '#fff',
	},

	screenContainerLogin: {
	  flex: 1,
	  flexDirection: 'column',
	  justifyContent: 'center',
	  alignItems: 'center',
	  backgroundColor: '#fff'
	},

	headerContainer:{
	  flex: 0,
	  justifyContent: 'center',
	  alignItems: 'center',
	  height: 120
	},

	headerContainerNoVideo: {
      flex: 1,
	  justifyContent: 'center',
	  alignItems: 'center',
	  marginTop:30	
	},

	footerContainer:{
	  flex: 2,
	  flexDirection: 'column',
	  backgroundColor: 'tomato'
	},

	infoText: {
	  flex: 1,
	  width: '90%',
	  height: '20%',
	  marginTop: '2%',
	  color: meditarColor,
	},

	scrollinfoText: {
	  flex: 1,
	  width: '90%',
	  height: '20%',
	  marginTop: '5%',
	  fontFamily: 'futurastd-book'
	},

	scrollText: {	
	  color: meditarColor,
	},

	title: {
	  color: meditarColor,
	  fontSize: 30
	},

	btn: {
	  backgroundColor: meditarColor,
	  width: 280,
	  height: 45
	},

	btnInverse: {
	  backgroundColor: '#fff',
	  width: 280,
	  height: 45,
	  borderColor: meditarColor,
	},

	formContainer: {
	  flex: 1,
	  justifyContent: 'center',
	  width: '100%',  
	  marginLeft: '5%', 
	  marginRight: '5%',
	},

	formContainerBtn: {
	  width: '100%',
	  flex: 1,
	  justifyContent: 'center',
	  alignItems: 'center',
	},

	space: {
	  height: 60,
	  width: '100%'
	},

	space40: {
	  height: 40,
	  width: '100%'
	}
}