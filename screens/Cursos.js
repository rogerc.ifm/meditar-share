import React from 'react';
import { Text, View, Image } from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
import { templateStyle } from './templateStyle';
import { AsyncStorage } from 'react-native';

const cursos = [
  {
    Title: 'Aprenda a meditarditar',
    content: 'Curso aprenda a meditar',
    Link: 'https://s3-eu-west-1.amazonaws.com/meditar/video.mp4'
  }
];

class Cursos extends React.Component {
  static navigationOptions = { header: null }

  constructor(props) {
    super(props);
  
    this.state = {
      data: cursos,
      pleno: false,
      btnText: 'Registrar'
    };
  }

  componentDidMount() {
    const willFocus = this.props.navigation.addListener(
      'willFocus',
      payload => {
        this.update();  
      }
    );

    this.update();
  }

  update = () => {
    AsyncStorage.getItem('pleno', (error, result) => {
      if (result === 'false') {
        console.log(result);
        console.log('* não é pleno *');  
        this.setState({pleno: false});  

      } else {
        
        console.log('* auth mural *');
        console.log('* fazer request *');
        this.setState({pleno: true});
      }
    });
  }

  render() {
    
    if(this.state.pleno){
      return (
        <View style={templateStyle.screenContainerNoHeader}>  
          
          <View style={templateStyle.headerContainer}>
            <Text style={templateStyle.title}>Cursos</Text>
          </View>
          <View>
            <List containerStyle={{marginBottom: 20}}>
              {
                this.state.data.map((entry, i) => (
                  <ListItem
                    key={i}
                    title={entry.Title}
                    
                    onPress={() => {
                      this.props.navigation.navigate('Curso', {
                        Title: `${entry.Title}`,
                        Link: `${entry.Link}`,
                        Type: `${entry.Type}`,
                        content: `${entry.content}`
                      });
                    }}
                  />
                ))
              }
            </List>
          </View>
        </View>
      );  
    }else {
      return (
        <View style={templateStyle.screenContainerNoHeader}>  
          <View style={templateStyle.headerContainerNoVideo}>
            <Image style={{width: 120, height: 150, marginTop: 50}} source={require('../assets/login_logo.png')}></Image>  
        
            <Text style={templateStyle.title}>Seja um usuário Pleno</Text>
            <Text style={templateStyle.infoText} >Registre-se e ganhe 7 dias de acesso pleno e o curso exclusivo aprenda a meditar</Text>
            <Button
              title={this.state.btnText}
              buttonStyle={templateStyle.btn}
              
              onPress={() => {
              this.props.navigation.navigate('Perfil');
              }}
            />
          </View>
        </View>
      );
    }    
  }
}

export { Cursos }

const title = {
  color: '#93669c',
  fontSize: 30,
  paddingTop: 10
}

const container = { 
  flex: 0, 
  justifyContent: 'center', 
  alignItems: 'center' 
}
