import React from 'react';
import Expo from 'expo';
import axios from 'axios';
import config from '../config/config'
import { Text, View, Image, ActivityIndicator, TouchableOpacity, Picker, Alert,StyleSheet, ScrollView, ImageBackground, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants } from 'expo';
import { templateStyle } from './templateStyle'
import { AsyncStorage } from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { loadSound, playSound, pauseSound, unloadSound } from '../helpers/sound'
import { Video, Audio } from 'expo';
import { Font } from 'expo';
import { Permissions, Notifications } from 'expo';
import "@expo/vector-icons";
import SvgUri from 'react-native-svg-uri';


const baseURL = config.api.URL;
const bearerToken = " ";

class Home extends React.Component {
  static navigationOptions = { header: null }

  constructor(props) {
    super(props);
    
    this.state = {
      data: {
        Title: '',
        Link: '',
        Type: '',
        content: '',
      }, 
      btnText: 'SEJA PLENO',
      loading: true,
      pleno: false,
      sound: '../assets/sound/drums.mp3',
      showPicker: false,
      som: 'Nenhum',
      playng: false,
      useNativeControls: true,
      contentLoaded: false,
      contentFreeLoaded: false,
      admin: false,
    };

    loadSound();
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

  }

  async componentDidMount() {
    Alert.alert('Remova o telefone do modo silencioso!');

    await Font.loadAsync({
      'dk-cinnabar-brush': require('../assets/fonts/dk-cinnabar-brush.ttf'),
      'futurastd-book': require('../assets/fonts/futurastd-book.ttf')
    });
    
    const willFocus = this.props.navigation.addListener(
      'willFocus',
      payload => {
        this.update();  
      }
    );
    
    this.setState({ fontLoaded: true });
    this.update();
    
    
  }

  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    if (finalStatus !== 'granted') {
      return;
    }

    let token = await Notifications.getExpoPushTokenAsync();
    
    return token;
  }

  trackingDevice() {
    
    (async () => {
        
        let ExponentPushToken = await this.registerForPushNotificationsAsync();
      
        let data = {
            device: ExponentPushToken,
            pleno: this.state.pleno,
            plenos: [],
            free:[]
          }

        axios.get(baseURL+'/Activities?filter=%7B%22order%22%3A%22createdAt%20DESC%22%2C%22limit%22%3A%201%7D')
        .then((response)=> {
          if (this.state.pleno === true) {
            this.register(response.data, data, true);
          } else {
            this.register(response.data, data, false);
          }
        })
        .catch((error)=> {
          console.log(error);
        });

    })();
  }
  
  register = (response, newData, plenou) => {
   let free = response[0].free;
   let plenos = response[0].plenos;
   
   if ( plenou ) {
    //console.log('*********** Plenou **************');
    plenos.push(newData.device);
    plenos = plenos.filter((item, pos) => {return plenos.indexOf(item) == pos;});
    free = free.filter(e => e !== newData.device);
   } 
   else {
    //console.log('********** Não plenou ***************')
    free.push(newData.device);
    free = free.filter((item, pos) => {return free.indexOf(item) == pos;});
    free = free.filter((item, pos) => {return free.indexOf(item) == pos;});
    plenos = plenos.filter(e => e !== newData.device);
   }
  
   newData.free = free;
   newData.plenos = plenos;

  axios.post(baseURL+'/Activities', newData)
  .then(function (response) {
    //console.log(response);
  })
  .catch(function (error) {
    //console.log(error);
  });

////
  }

  update() {
    AsyncStorage.getItem('userId', (error, result) => {
        console.log(result);

        if ( result === config.api.MASTER ) {
          this.setState({admin: true});
        } else {
          this.setState({admin: false});
        }
    });


    AsyncStorage.getItem('token', (error, result) => {
        if (result === '' ) {
          this.setState({pleno: false});
        }else {
          this.setState({token: result});
        }
    });

    AsyncStorage.getItem('pleno', (error, result) => {
      if (result === 'false' || result === null ) {
        
        this.setState({pleno: false});  
        this.setState({'loading': true});
        this.setState({contentLoaded:false});
        this.setState({contentFreeLoaded:true});
        this.getLastContent();
        
      
      } else {
        this.setState({contentFreeLoaded:false});
        this.setState({pleno: true});
        this.setState({'loading': true});
        this.setState({contentLoaded:true});
        this.getLastContentPremium(this.state.token);
      }
    });

    this.trackingDevice()
  }

  getLastContent() {  
    axios.get(baseURL+'/Posts/findOne?filter=%7B%20%22order%22%3A%20%22createdAt%20DESC%22%20%7D')
    .then((response) => {
      this.setState({data: response.data});
      setTimeout(()=>{this.setState({loading: false})}, 2000);
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  getLastContentPremium(token) {   
    axios.get(baseURL+'PostPlenos/findOne?filter=%7B%20%22order%22%3A%20%22createdAt%20DESC%22%7D&access_token='+token)
    .then((response) => {
      this.setState({data: response.data});
      setTimeout(()=>{this.setState({loading: false})}, 2000);
    })
    .catch((error)=> {
      console.log(error);
    });
  }

  controlSound = (playbackStatus) =>{
    
    if (playbackStatus.isPlaying){
      this.setState({playng: true});
      Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.ALL);
    } else {
      this.setState({playng: false});
      Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    }

    if (this.state.som === 'Nenhum') {
      pauseSound();
    } else {
      
      if (playbackStatus.isPlaying) {
        //pauseSound();
        playSound(this.state.som);
        
        return;
      } else {
        pauseSound();
        
      }

    }

  }

  showPicker = () => {
    if (this.state.playng) {
      Alert.alert('Pause o video!');
      return;
    }
    this.setState({showPicker: true})
  }

  changeSom = (type) => {
    if (this.state.playng) {
      Alert.alert('Pause o video!');
      return;
    }
    
    if (type === this.state.som) {
      Alert.alert('Pausando som de fundo');
      this.setState({som: 'Nenhum'});
      return;      
    }

    this.setState({som: type})
  }

  
  hidePicker = () => {
    this.setState({showPicker: false});
  }

  getUserinfo = (id, token) => {
 
    let url = config.api.URL+'Accounts/'+id+'?access_token='+token

    axios.get(url)
     .then( (response) => {
       this.setState({userData: response.data});
     })
     .catch(function (error) {
       console.log(error);
     });
  }

  handleVideoMount = ref => {
    this.player = ref;
  };

  handleToggleUseNativeControls = () =>
    this.setState({ useNativeControls: !this.state.useNativeControls });

  handlePlaybackStatusUpdate = async status => {
    if (status.didJustFinish) {
      await this.player.setPositionAsync(0);
      this.setState({ shouldPlay: false });
    }
  };



  render() {     
    if(this.state.loading === true){
      return(
        <View style={{flex: 1, alignItems: 'center', height: '100%', justifyContent: 'center'}}>
          <ActivityIndicator size="large" color="#6a56b1" />
        </View>
      )
    } else {

      return (
        <ImageBackground source={require('../assets/fundo.png')} style={templateStyle.screenContainerLogin}>      
                  
          {
            this.state.fontLoaded ? (
              <Video
                rate={0}
                resizeMode="contain"
                source={{ uri: this.state.data.Link}}
                ref={r=>this.vid =r}
                shouldPlay={false}
                useNativeControls={true}
                onPlaybackStatusUpdate={this.handlePlaybackStatusUpdate}
                onPlaybackStatusUpdate={(playbackStatus) => {this.controlSound(playbackStatus);}}
                style={{ width: '100%', height: '36%', marginTop: '10%', marginBottom: '5%' }}
              />
            ) : null
          }

          {
            this.state.fontLoaded ? (
              <Text style={{ fontFamily: 'dk-cinnabar-brush', fontSize: 20, color: '#fff' }}>
                .................. Sons de Fundo ..................
              </Text>
            ) : null
          }
          
          <View style={{width:'100%', height: 90, flex: 0, flexDirection: 'row', paddingLeft: '5%', paddingRight: '5%', paddingTop: '5%', justifyContent: 'space-between'}}>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('tambores')}}>
                <Image source={require('../assets/tambor.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('flauta')}}>
                <Image source={require('../assets/flauta.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('binatural')}}>
                <Image source={require('../assets/binatural.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('agua')}}>
                <Image source={require('../assets/agua.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
          </View>   

          {
            this.state.fontLoaded ? (
              <ScrollView style={templateStyle.scrollinfoText}>
                <Text style={styles.scrollText}>{this.state.data.content}</Text>
              </ScrollView>
            ) : null
          }
          
          {
            this.state.pleno === false &&
            <View style={{flexDirection: 'row', width:'100%', alignItems: 'center', height: 90, flex: 1, paddingLeft: '5%', paddingRight: '5%', paddingTop: '2%', justifyContent: 'center'}}>
             <TouchableOpacity style={{width: 200, flex: 0, alignItems: 'center',flexDirection: 'row'}} onPress={()=>{this.props.navigation.navigate('NewUser')}}>
                <Image source={require('../assets/pleno.png')} style={{width:80, height: 80}}/>
                 {
                  this.state.fontLoaded ? (
                    <Text style={{width: 100, color: '#fff', fontFamily: 'futurastd-book', fontSize: 16}}>...Seja Pleno</Text>
                  ) : null
                 }
             </TouchableOpacity>
            </View>
          }
          
          {
            this.state.admin &&
            <View style={{flexDirection: 'row', width:'100%', alignItems: 'center', height: 90, flex: 1, paddingLeft: '5%', paddingRight: '5%', paddingTop: '2%', justifyContent: 'center'}}>
              <Button
                title={`Administrar Conteúdo`}
                buttonStyle={templateStyle.btnInverse}
                textStyle={{color:templateStyle.meditarColor}}
                onPress={() => {
                this.props.navigation.navigate('AdminHome');
                }}
              />            
            </View>
          }

        </ImageBackground>
      );
    }

  }
}

export default Home;

const pickerContainer = {
  flex: 1,
  flexDirection: 'row',
  alignItems: "center",
};

const okBtn = {
  backgroundColor: '#6a56b1',
  height: 45,
  borderColor: '#6a56b1',
  borderWidth: 15,
  borderRadius: 30
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#000',
    paddingTop: Constants.statusBarHeight,
  },

  container2: {
    flex: 1,
    marginTop:60
  },

  imageContainer: {
    height:50,
    width: 50,
    borderRadius: 20
  },

  image: {
    height:50,
    width: 50,
    borderRadius: 50
  },

  scrollText: {
    flex: 1,
    width: '100%',
    height: '20%',
    marginTop: '5%',
    fontFamily: 'futurastd-book',
    color: '#fff'
  },

  video: {
    flex: 1,
  },

  buttons: {
    height: 64,
    flexDirection: 'row',
  },

  button: {
    flex: 1,
    margin: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },

  buttonCircular: {
    flex: 0,
    paddingLeft: 16,
    justifyContent: 'center',
    width: 65,
    height: 65,
    backgroundColor: 'transparent',
    borderRadius: 65 / 2,
    padding: 10,
    marginBottom: 20,
    shadowRadius: 10,
    borderColor: 'white',
    borderWidth: 1
  },

  buttonText: {
    textAlign: 'center',
    backgroundColor: 'transparent',
  },

  header: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  logBlock: {
    height: 140,
  },

  logs: {
    flex: 1,
    color: '#FFF',
    marginTop: 10,
    fontFamily: 'monospace',
  },
});
