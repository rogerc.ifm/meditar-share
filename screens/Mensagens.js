import React from 'react';
import { List, ListItem } from 'react-native-elements';
import { templateStyle } from './templateStyle';
import { Text, View, Image,ActivityIndicator, AsyncStorage, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants, Video } from 'expo';
import { withNavigationFocus } from 'react-navigation';

import VideoPlayer from '@expo/videoplayer';
import axios from 'axios';
import config from '../config/config'
import "@expo/vector-icons";
import Expo from 'expo';
Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

const baseURL = config.api.URL;

class Mensagens extends React.Component {
  static navigationOptions = { header: null }

  constructor(props) {
    super(props);
    
    this.state = {
      data: [], 
      loading: false,
      pleno: false
    };
  }

  componentDidMount() {
    const willFocus = this.props.navigation.addListener(
      'willFocus',
      payload => {
        this.setState({'loading': true});
        this.update();
      }
    );

    this.update();
  }

  update() {
      AsyncStorage.getItem('userId', (error, result) => {
        console.log(result);

        if ( result === config.api.MASTER ) {
          this.setState({admin: true});
        } else {
          this.setState({admin: false});
        }
    });


    AsyncStorage.getItem('token', (error, result) => {
        if (result === '' ) {
          this.setState({pleno: false});
        }else {
          this.setState({token: result});
        }
    });

    AsyncStorage.getItem('pleno', (error, result) => {
      if (result === 'false' || result === null ) {
        
        this.setState({pleno: false});  
        this.setState({'loading': true});
        this.setState({contentLoaded:false});
        this.setState({contentFreeLoaded:true});
        this.getLastContent();
        
      
      } else {
        this.setState({contentFreeLoaded:false});
        this.setState({pleno: true});
        this.setState({'loading': true});
        this.setState({contentLoaded:true});
        this.getLastContentPremium(this.state.token);
      }
    });

  }


  getLastContent() {      
    axios.get(baseURL+'Mensagems?filter=%7B%20%22order%22%3A%20%22createdAt%20DESC%22%7D')
    .then((response) =>{
      this.setState({data: response.data.docs});
      this.setState({'loading': false});
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  getLastContentPremium() {      
    axios.get(baseURL+'MensagemPlenos?filter=%7B%20%22order%22%3A%20%22createdAt%20DESC%22%7D')
    .then((response) =>{
      this.setState({data: response.data.docs});
      this.setState({'loading': false});
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    if(this.state.loading){
      return(
        <View style={{flex: 1, alignItems: 'center', height: '100%', justifyContent: 'center'}}>
          <ActivityIndicator size="large" color="#6a56b1" />
        </View>
      )
    } else {
      return (
      <View style={templateStyle.screenContainerNoHeader}>  
        <View style={templateStyle.headerContainer}>
          <Text style={{ fontFamily: 'dk-cinnabar-brush', fontSize: 30, color: '#6a56b1' }}>Mensagens</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        <View>
            <List containerStyle={{marginBottom: 20}}>
              {
                this.state.data.map((entry, i) => (
                  <ListItem
                    key={i}
                    title={entry.Title}
                    
                    onPress={() => {
                      this.props.navigation.navigate('Mensagem', {
                        Title: `${entry.Title}`,
                        content: `${entry.content}`,
                        contentLink: `${entry.contentLink}`
                      });
                    }}
                  />
                ))
              }
            </List>
        </View>
        </ScrollView>
   
      </View>
    );
    }
  }
}

export { Mensagens }
