import React from 'react';
import { Text, View, Image, Alert, AsyncStorage, KeyboardAvoidingView, ImageBackground } from 'react-native';

import { templateStyle } from './templateStyle';
import { Card, ListItem, Button, FormLabel, FormInput } from 'react-native-elements';
import config from '../config/config';
import axios from 'axios';

const baseURL = config.api.URL;
let bearerToken = '';

const users = [
 {
    name: 'brynn',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
 }
]

class NewUser extends React.Component {
  static navigationOptions = { headerTitle: "Novo usuário" }
  
  constructor(props) {
    super(props);
    
    this.state = { 
        logged: false
    }
  }

  componentDidMount(){

    AsyncStorage.getItem('userId', (error, result) => {
        console.log('here');
        if (result === null) {
          console.log(result);
          console.log('no token');  
        } else {
          console.log('componentDidMount');
          this.setState({logged: true});
          this.getUserinfo(result);
        }
    });
  }

  setEmail = (e) => {
    let stateChange = this.state;
    
    stateChange.data.email = e.target.value;
    this.setState(stateChange);
  }

  setPassword = (e) => {
    let stateChange = this.state;    
    stateChange.data.password = e.target.value;
    this.setState(stateChange);
  }

  login = (e) => {
    if (this.state.email === '' || this.state.password === ''){
      return;
    }
    this.submit();
  }

  out = (e) => {
    AsyncStorage.setItem('token', '');
    AsyncStorage.setItem('userId', '');
    AsyncStorage.setItem('pleno', 'false');
    this.setState({logged: false});
  }

  getUserinfo = (id, token) => {
    axios.get(config.api.URL+'Accounts/'+id+'?access_token='+bearerToken )
    .then( (response) => {
      this.setState({userData: response.data});
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  submit = () => {
    let data ={
      firstname: this.state.firstname,
      email: this.state.email,
      cpf: this.state.cpf,
      password: this.state.password,
      passwordc: this.state.passwordc,
      pleno: 'true'
    }
    console.log(data);
    const buttons = {};

    if ( data.firstname === undefined || data.email === undefined || data.password === undefined ) {
      Alert.alert('Complete todos os campos');
      return;
    }

    if (data.password !== data.passwordc ){
      Alert.alert('Campos de senha não correspondem');
      this.setState({password: undefined, passwordc: undefined, })
      return; 
    }

    if (!this.validateEmail(data.email)){
      Alert.alert('Campos de email inválido');
      return;
    }

    if (data.password.length < 6){
      Alert.alert('Senha muito curta');
      return;
    }

    axios.post(baseURL+'Accounts?access_token='+config.api.token, data)
    .then( (response) => {
      Alert.alert('Usuário criado com sucesso');
      this.props.navigation.navigate('Perfil');
    })
    .catch(function (error) {
      Alert.alert('Tente Novamente...');
      console.log(error);
    });
  }

  render() {
    
    if (this.state.logged === false ){
      return (
      <ImageBackground source={require('../assets/login.png')} style={templateStyle.screenContainerLogin} behavior="padding">         

        <KeyboardAvoidingView style={formContainer} behavior="padding">
          
          <FormLabel labelStyle={{color: '#fff'}}>Nome</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{ color: '#fff'}} onChangeText={(firstname) => this.setState({firstname})}/>
          <FormLabel labelStyle={{color: '#fff'}}>Email</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{ color: '#fff'}} onChangeText={(email) => this.setState({email})}/>
          <FormLabel labelStyle={{color: '#fff'}}>CPF</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{ color: '#fff'}} onChangeText={(cpf) => this.setState({cpf})}/>
          <FormLabel labelStyle={{color: '#fff'}} secureTextEntry>Senha</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{ color: '#fff'}} onChangeText={(password) => this.setState({password})} secureTextEntry/>
          <FormLabel labelStyle={{color: '#fff'}} secureTextEntry>Confirmar Senha</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{ color: '#fff'}} onChangeText={(passwordc) => this.setState({passwordc})} secureTextEntry/>
        
          <View style={btnContainer}>
            <Button  title={`SALVAR`}  buttonStyle={btn} onPress={this.submit} />
          </View>  
        
        </KeyboardAvoidingView>
          
      </ImageBackground>
      
    );  
    } else {
      return (
        <View style={templateStyle.screenContainer}></View>
      );
    }

    
  }
}

export { NewUser }

const btn = {
  backgroundColor: '#6a56b1',
  width: 280,
  height: 45,
  marginTop: 20
}

const formContainer = {
    flex: 1,
    width: '100%',  
    marginLeft: '5%', 
    marginRight: '5%'
}

const btnContainer = {
  flex: 1,
  width: '100%',
  alignItems: 'center',
}