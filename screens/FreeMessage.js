import React from 'react';
import { Text, View, Image, Alert, AsyncStorage, KeyboardAvoidingView, ImageBackground, Keyboard } from 'react-native';
import { templateStyle } from './templateStyle';
import { Card, ListItem, Button, FormLabel, FormInput } from 'react-native-elements';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import config from '../config/config';
import axios from 'axios';

const baseURL = config.api.URL;
const TOKEN = config.api.token;

let bearerToken = '';

class FreeMessage extends React.Component {
  static navigationOptions = { headerTitle: "Mensagem Free" }
  
  constructor(props) {
    super(props);
    
    this.state = { 
        logged: false,
        loadEnd: true,
        message: '',
        title: '',
        push: [],
    }
  }

  componentDidMount(){
    axios.get(baseURL+'/Activities?filter=%7B%22order%22%3A%22createdAt%20DESC%22%2C%22limit%22%3A%201%7D')
    .then((response)=> {
      this.setState({push: response.data[0].free});
    })
    .catch((error)=> {
      console.log(error);
    });
  }

  setEmail = (e) => {
    let stateChange = this.state;
    
    stateChange.data.email = e.target.value;
    this.setState(stateChange);
  }

  setPassword = (e) => {
    let stateChange = this.state;    
    stateChange.data.password = e.target.value;
    this.setState(stateChange);
  }

  login = (e) => {
    if (this.state.email === '' || this.state.password === ''){
      return;
    }
    this.submit();
  }

  out = (e) => {
    AsyncStorage.setItem('token', '');
    AsyncStorage.setItem('userId', '');
    AsyncStorage.setItem('pleno', 'false');
    this.setState({logged: false});
  }

  getUserinfo = (id, token) => {
    axios.get(config.api.URL+'Accounts/'+id+'?access_token='+bearerToken )
    .then( (response) => {
      this.setState({userData: response.data});
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  submit = () => {
    let data = {
      message: this.state.message,
      title: this.state.title
    }

    let freemessage = {
      Title: this.state.title,
      content: this.state.message
    }
    
    if ( data.message === '' || data.title === '') {
      Alert.alert('Complete todos os campos');
      return;
    }

    axios.post(baseURL+'/Mensagems?access_token='+TOKEN, freemessage)
      .then(function (response) {
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error); 
      });


    this.state.push.map((entry)=>{
      let content = {
        "to": entry,
        "title": "Mensagem Adicionada:"+this.state.title,
        "body": this.state.message
      }

      axios.post('https://exp.host/--/api/v2/push/send', content, {
          headers: {
              'Content-Type': 'application/json',
          }
        })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error); 
          alert(error)
        });

    })
    this.input.clearText();
    this._textInput.clear();
    Alert.alert('Push enviado');
  }
  
  componentWillMount () {
    this.content = (
      <KeyboardAvoidingView style={formContainer} behavior="padding">
        <FormLabel labelStyle={{color: '#fff'}}>Título</FormLabel>
        <FormInput ref={input => this.input = input} inputStyle={{ color: '#fff'}} onChangeText={(title) => this.setState({title})}/>
        <View style={{flexDirection: 'column', width:'100%', alignItems: 'center', height: 'auto', flex: 1, justifyContent: 'space-between', paddingBottom: 70, paddingTop: 70}}>
          <AutoGrowingTextInput
            onChangeText={(message) => this.setState({message})} 
            style={textInput}
            placeholder={'Escreva aqui a Mensagem'}
            placeholderTextColor='#fff'
            maxHeight={200}
            enableScrollToCaret
            ref={(r) => { this._textInput = r; }}
          />
          <View style={btnContainer}>
            <Button  title={`Enviar`}  buttonStyle={btn} onPress={this.submit} />
          </View>          
        </View>

      </KeyboardAvoidingView>
    )
  }

  render() {  
    if (this.state.loadEnd === true ){
      return (
      <ImageBackground source={require('../assets/login.png')} style={templateStyle.screenContainerLogin} behavior="padding">         
        {this.content}
      </ImageBackground>
    );  
    } else {
      return (
        <View style={templateStyle.screenContainer}></View>
      );
    }
  }
}

export { FreeMessage }

const btn = {
  backgroundColor: '#6a56b1',
  width: 280,
  height: 45,
  marginTop: 20
}

const formContainer = {
  flex: 1,
  width: '100%',  
  marginLeft: '5%', 
  marginRight: '5%'
}

const btnContainer = {
  flex: 1,
  width: '100%',
  alignItems: 'center',
}

const textInput =  {
  paddingLeft: 10,
  fontSize: 17,
  flex: 1,
  backgroundColor: 'transparent',
  borderWidth: 0,
  color: '#fff'
}