import React from 'react';
import { List, ListItem } from 'react-native-elements';
import { templateStyle } from './templateStyle';
import { Text, View, Image,ActivityIndicator, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants, Video } from 'expo';
import { withNavigationFocus } from 'react-navigation';

import VideoPlayer from '@expo/videoplayer';
import axios from 'axios';
import config from '../config/config'
import "@expo/vector-icons";
import Expo from 'expo';
Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);


const baseURL = config.api.URL;

class AdminHome extends React.Component {
  static navigationOptions = { headerTitle: "Administrar conteúdo" }

  constructor(props) {
    super(props);
    
    this.state = {
      data: [], 
      loading: false,
      pleno: false,
      contentFreeLoaded: false
    };
  }

  render() {
    if(this.state.loading){
      return(
        <View style={{flex: 1, alignItems: 'center', height: '100%', justifyContent: 'center'}}>
          <ActivityIndicator size="large" color="#6a56b1" />
        </View>
      )
    } else {
      return (
      <View style={templateStyle.screenContainerNoHeader}>  
              
        <View style={{flexDirection: 'column', width:'100%', alignItems: 'center', height: 'auto', flex: 1, justifyContent: 'space-between', paddingBottom: 70, paddingTop: 70}}>
          <Button
            title={`Push Genérico`}
            buttonStyle={templateStyle.btn}
            onPress={() => {
            this.props.navigation.navigate('GenericPush');
            }}
          />
          <Button
            title={`Mensagem Free`}
            buttonStyle={templateStyle.btn}
            onPress={() => {
            this.props.navigation.navigate('FreeMessage');
            }}
          />
          <Button
            title={`Video Free`}
            buttonStyle={templateStyle.btn}
            onPress={() => {
            this.props.navigation.navigate('PostFree');
            }}
          />
          <Button
            title={`Video Pleno`}
            buttonStyle={templateStyle.btn}
            onPress={() => {
            this.props.navigation.navigate('PostPleno');
            }}
          />            
          <Button
            title={`Mensagem Pleno`}
            buttonStyle={templateStyle.btn}
            onPress={() => {
            this.props.navigation.navigate('PlenoMessage');
            }}
          />
        </View>
   
      </View>
    );
    }
  }
}

export { AdminHome }
