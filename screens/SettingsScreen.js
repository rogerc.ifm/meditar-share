import React from 'react';
import moment from 'moment'
import { AsyncStorage, Text, View, Image, Alert, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { Card, ListItem, Button, FormLabel, FormInput } from 'react-native-elements';
import { templateStyle } from './templateStyle';
import config from '../config/config';
import axios from 'axios';

const baseURL = config.api.URL;
let bearerToken = '';

class SettingsScreen extends React.Component {
  static navigationOptions = { headerTitle: "Seja Pleno", backgroundColor: 'transparent'}

  constructor(props) {
    super(props);
    
    this.state = { 
        logged: false,
        token: '',
        userData: {
          firstname: '',
          lastname:  '',
          city: '',
          createdAt: '',
          email: ''
        }
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('pleno', (error, result) => {
        console.log(result);
    });

    AsyncStorage.getItem('userId', (error, result) => {
        if (result === null) {
          this.setState({userId: result});
        } else {
          this.setState({userId: result});
          this.setState({logged: true});
        }
    });

    AsyncStorage.getItem('token', (error, result) => {
        
        if (result === null) {
          this.setState({token: result});
        } else {
          this.setState({logged: true});
          this.setState({token: result});
          this.getUserinfo(this.state.userId, this.state.token);
        }
    });
  }

  setEmail = (e) => {
    let stateChange = this.state;
    
    stateChange.data.email = e.target.value;
    this.setState(stateChange);
  }

  setPassword = (e) => {
    let stateChange = this.state;    
    stateChange.data.password = e.target.value;
    this.setState(stateChange);
  }

  login = (e) => {
    if (this.state.email === '' || this.state.password === ''){
      return;
    }
    this.submit();
  }

  out = (e) => {
    AsyncStorage.setItem('token', '');
    AsyncStorage.setItem('userId', '');
    AsyncStorage.setItem('pleno', 'false');
    this.setState({logged: false});
  }

  getUserinfo = (id, token) => {
    let url = config.api.URL+'Accounts/'+id+'?access_token='+token

    axios.get(url)
     .then( (response) => {
       this.setState({userData: response.data});
     })
     .catch(function (error) {
       console.log(error);
     });
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  submit = () => {
    let data = {
      email: this.state.email,
      password: this.state.password
    }

    if ( data.email === undefined || data.password === undefined ) {
      Alert.alert('Complete todos os campos');
      return;
    }

    if (!this.validateEmail(data.email)){
      Alert.alert('Campo de email em formato inválido');
      return;
    }

    axios.post(config.api.URL+'Accounts/login', data)
    .then( (response) => {
      AsyncStorage.setItem('token', response.data.id);
      AsyncStorage.setItem('userId', response.data.userId);
      AsyncStorage.setItem('pleno', response.data.userId);
      
      this.setState({logged: true});
      this.getUserinfo(response.data.userId, response.data.id );
    })
    .catch(function (error) {
      Alert.alert('Usuário Inválido');
      console.log(error);
    });
  }

  render() {
    
    if (this.state.logged === false ){
      return (
      <ImageBackground source={require('../assets/login.png')} style={templateStyle.screenContainerLogin} behavior="padding">      
        
        <KeyboardAvoidingView style={formContainer}>
          <FormLabel labelStyle={{color: '#fff'}}>Email</FormLabel>
          <FormInput autoCapitalize='none' inputStyle={{color: '#fff'}} onChangeText={(email) => this.setState({email})}/>
          <FormLabel labelStyle={{color: '#fff'}}>Password</FormLabel>
          <FormInput autoCapitalize='none'inputStyle={{color: '#fff'}} onChangeText={(password) => this.setState({password})} secureTextEntry/>
       
          <View style={btnContainer}>
            <Button 
              title={`LOGIN`}  
              buttonStyle={templateStyle.btn}
              onPress={this.submit}
            />
            <View style={{marginTop: 20}} />

            <Button
              title={`REGISTRE-SE`}
              buttonStyle={templateStyle.btnInverse}
              textStyle={{color:templateStyle.meditarColor}}
              onPress={() => {
              this.props.navigation.navigate('NewUser');
              }}
            />
        </View>

        </KeyboardAvoidingView>
        
          
        
      </ImageBackground>
    );  
    } else {

      const since = moment(this.state.userData.createdAt).format('MMMM Do YYYY')

      return (
        <View style={templateStyle.screenContainer}>      
          <View style={templateStyle.space} />

          <Card title="Informações do usuário">
              <View>
                <Text>Meditar.com.{this.state.userData.firstname}</Text>
                <View style={templateStyle.space40} />
                <Text >{this.state.userData.email || ''}</Text>
                <Text >{'Ativo desde: '+since || ''}</Text>
                <Text >{this.state.userData.city || ''}</Text>
              </View>
          </Card>

          <View style={templateStyle.space} />
          <View style={templateStyle.formContainer}>
            <Button 
              title={`LOG-OUT`}  
              buttonStyle={templateStyle.btnInverse}
              textStyle={{color:templateStyle.meditarColor}}
              onPress={this.out}
            />
          </View> 
        </View>
      );
    }  
  }
}

export { SettingsScreen }     

const formContainer = {
    flex: 1,
    width: '100%',  
    marginLeft: '5%', 
    marginRight: '5%'
}

const btnContainer = {
  flex: 1,
  width: '100%',
  alignItems: 'center',
  marginTop: 40
}

const formContainerBtn = {
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
}