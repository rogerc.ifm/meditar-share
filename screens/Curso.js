import React from 'react';
import VideoPlayer from '@expo/videoplayer';
import { Text, View, Image, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants, Video } from 'expo';
import { templateStyle } from './templateStyle';
import "@expo/vector-icons";


class Curso extends React.Component {
  render() {
    const { params } = this.props.navigation.state;
    const Title = params ? params.Title : '';
    const Link = params ? params.Link : '';
    const Type = params ? params.Type : '';
    const content = params ? params.content : '';

    return (
      <View style={templateStyle.screenContainer}>      
        <View style={templateStyle.headerContainer}>
          <Text style={templateStyle.title}>{Title}</Text>
        </View>
        <VideoPlayer
          videoProps={{
            shouldPlay: false,
            resizeMode: Video.RESIZE_MODE_CONTAIN,
            source: {
              uri: Link,
            },
          }}
          isPortrait={true}
          playFromPositionMillis={0}
        />
        <View style={templateStyle.footerContainer}>
          <Text style={templateStyle.infoText} >{content}</Text>
        </View>
      </View>
    );
  }
}

export { Curso }