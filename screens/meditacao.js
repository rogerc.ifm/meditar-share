import React from 'react';
import VideoPlayer from '@expo/videoplayer';
import { Text, View, Image, ActivityIndicator, TouchableOpacity, Picker, Alert,StyleSheet, ScrollView, ImageBackground, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants, Video } from 'expo';
import { templateStyle } from './templateStyle';
import { loadSound, playSound, pauseSound, unloadSound } from '../helpers/sound';
import "@expo/vector-icons";
import Expo from 'expo';
Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);


class Meditacao extends React.Component {
   static navigationOptions = { headerTitle: "Meditar.com.vc" }
  
   constructor(props) {
    super(props);
    
    this.state = {
      data: {
        Title: '',
        Link: '',
        Type: '',
        content: '',
      }, 
      btnText: 'SEJA PLENO',
      loading: true,
      pleno: false,
      sound: '../assets/sound/drums.mp3',
      showPicker: false,
      som: 'Nenhum',
      playng: false,
      useNativeControls: true,
      contentLoaded: false,
      contentFreeLoaded: false,
      fontLoaded: false,
    };

    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
  } 

   controlSound = (playbackStatus) =>{
    
    if (playbackStatus.isPlaying){
      this.setState({playng: true});
      Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.ALL);
    } else {
      this.setState({playng: false});
      Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    }

    if (this.state.som === 'Nenhum') {
      pauseSound();
    } else {
      
      if (playbackStatus.isPlaying) {
        playSound(this.state.som);
        
        return;
      } else {
        pauseSound();
        
      }

    }

  }

  showPicker = () => {
    if (this.state.playng) {
      Alert.alert('Pause o video!');
      return;
    }
    this.setState({showPicker: true})
  }

  handleVideoMount = ref => {
    this.player = ref;
  };

  handleToggleUseNativeControls = () =>
    this.setState({ useNativeControls: !this.state.useNativeControls });

  handlePlaybackStatusUpdate = async status => {
    if (status.didJustFinish) {
      await this.player.setPositionAsync(0);
      this.setState({ shouldPlay: false });
    }
  };

  async componentDidMount() {
    console.log('here');

    await Font.loadAsync({
      'dk-cinnabar-brush': require('../assets/fonts/dk-cinnabar-brush.ttf'),
      'futurastd-book': require('../assets/fonts/futurastd-book.ttf')
    });
    
    this.setState({ fontLoaded: true });

  }
  
  changeSom = (type) => {
    if (this.state.playng) {
      Alert.alert('Pause o video!');
      return;
    }
    
    if (type === this.state.som) {
      Alert.alert('Pausando som de fundo');
      this.setState({som: 'Nenhum'});
      return;      
    }

    this.setState({som: type})
  }


  render() {
    const { params } = this.props.navigation.state;
    const Title = params ? params.Title : '';
    const Link = params ? params.Link : '';
    const Type = params ? params.Type : '';
    const content = params ? params.content : '';

    return (
      <ImageBackground source={require('../assets/fundo.png')} style={styles.screenContainerLogin}>      
                  
          <Video
            rate={0}
            resizeMode="contain"
            source={{ uri: Link}}
            ref={r=>this.vid =r}
            shouldPlay={false}
            useNativeControls={true}
            onPlaybackStatusUpdate={this.handlePlaybackStatusUpdate}
            onPlaybackStatusUpdate={(playbackStatus) => {this.controlSound(playbackStatus);}}
            style={{ width: '100%', height: '36%', marginBottom: '5%' }}
          />
           

          <Text style={{ fontFamily: 'dk-cinnabar-brush', fontSize: 20, color: '#fff' }}>
            .................. Sons de Fundo ..................
          </Text>
     
          <View style={{width:'100%', height: 90, flex: 0, flexDirection: 'row', paddingLeft: '5%', paddingRight: '5%', paddingTop: '5%', justifyContent: 'space-between'}}>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('tambores')}}>
                <Image source={require('../assets/tambor.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('flauta')}}>
                <Image source={require('../assets/flauta.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('binatural')}}>
                <Image source={require('../assets/binatural.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
             <TouchableOpacity style={styles.buttonCircular} onPress={()=>{this.changeSom('agua')}}>
                <Image source={require('../assets/agua.png')} style={{width:30, height: 30}}/>
             </TouchableOpacity>
          </View>   
             

        </ImageBackground>
    );
  }
}

export { Meditacao }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#000',
    paddingTop: Constants.statusBarHeight,
  },

  screenContainerLogin: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#fff'
  },

  container2: {
    flex: 1,
    marginTop:60
  },

  imageContainer: {
    height:50,
    width: 50,
    borderRadius: 20
  },

  image: {
    height:50,
    width: 50,
    borderRadius: 50
  },

  scrollText: {
    flex: 1,
    width: '100%',
    height: '20%',
    marginTop: '5%',
    fontFamily: 'futurastd-book',
    color: '#fff'
  },

  video: {
    flex: 1,
  },

  buttons: {
    height: 64,
    flexDirection: 'row',
  },

  button: {
    flex: 1,
    margin: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },

  buttonCircular: {
    flex: 0,
    paddingLeft: 16,
    justifyContent: 'center',
    width: 65,
    height: 65,
    backgroundColor: 'transparent',
    borderRadius: 65 / 2,
    padding: 10,
    marginBottom: 20,
    shadowRadius: 10,
    borderColor: 'white',
    borderWidth: 1
  },

  buttonText: {
    textAlign: 'center',
    backgroundColor: 'transparent',
  },

  header: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  logBlock: {
    height: 140,
  },

  logs: {
    flex: 1,
    color: '#FFF',
    marginTop: 10,
    fontFamily: 'monospace',
  },
});
