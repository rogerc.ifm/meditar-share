import React from 'react';
import VideoPlayer from '@expo/videoplayer';
import { Text, View, Image, StyleSheet, Picker, Alert, ScrollView, Linking } from 'react-native';
import { Button } from 'react-native-elements';
import { Constants, Video } from 'expo';
import { templateStyle } from './templateStyle';
import { loadSound, playSound, pauseSound, unloadSound } from '../helpers/sound';
import "@expo/vector-icons";
import Expo from 'expo';
Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);


class Mensagem extends React.Component {
   
  render() {
    const { params } = this.props.navigation.state;
    const Title = params ? params.Title : '';
    const content = params ? params.content : '';
    let contentLink = params ? params.contentLink : '';

    if (contentLink === 'undefined') {
      contentLink = 'http://meditar.com.vc';
    }

    return (
      <View style={templateStyle.screenContainerLogin}>      
        <Text style={templateStyle.title}>{Title}</Text>
        
        <ScrollView style={templateStyle.scrollinfoText}>
          <Text selectable={true} style={templateStyle.scrollText}>{content}</Text>
          <Text style={{marginTop: 20, color:'#6a56b1'}} onPress={ ()=>{ Linking.openURL(contentLink)}} >Saiba mais</Text>
        </ScrollView>

      </View>
    );
  }
}

export { Mensagem }

const headerContainer = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
}

const title = {
  color: '#6a56b1',
  fontSize: 30,
  marginTop: '2%',
}
