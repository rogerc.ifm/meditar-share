import Expo from 'expo';

const SOUNDS = {};
const soundObject = new Expo.Audio.Sound();
const soundObject2 = new Expo.Audio.Sound();
const soundObject3 = new Expo.Audio.Sound();
const soundObject4 = new Expo.Audio.Sound();

export async function loadSound(source) {
	try {
		await soundObject.loadAsync(require('./tambores.m4a'));
	} 
	catch (error) {
		console.log(error);
	}
	try {
		await soundObject2.loadAsync(require('./agua.mp3'));
	}
	catch (error) {
		console.log(error);
	}
	try {
		await soundObject3.loadAsync(require('./binatural.m4a'));
	}
	catch (error) {
		console.log(error);
	}

	try {
		await soundObject4.loadAsync(require('./flauta.m4a'));	
	}

	catch (error) {
		console.log(error);
	}
}


export async function playSound(source) {

	if( source === 'tambores' ){
		
		try {
			await soundObject.playAsync();
			await soundObject.setVolumeAsync(0.1);
			await soundObject.setIsLoopingAsync(true);
		} catch (error) {
			console.log(error);
		}
	}
	if( source === 'agua' ){
		try {
			await soundObject2.playAsync();
			await soundObject2.setVolumeAsync(0.1);
			await soundObject2.setIsLoopingAsync(true);
		} catch (error) {
			console.log(error);
		}
	}
	if( source === 'binatural' ){
	
		try {
			await soundObject3.playAsync();
			await soundObject3.setVolumeAsync(0.1);
			await soundObject3.setIsLoopingAsync(true);
		} catch (error) {
			console.log(error);
		}
	}
	if( source === 'flauta' ){
	
		try {
			await soundObject4.playAsync();
			await soundObject4.setVolumeAsync(0.1);
			await soundObject4.setIsLoopingAsync(true);
		} catch (error) {
			console.log(error);
		}
	}		

}

export async function pauseSound(source) {
	try {
		await soundObject.pauseAsync();
		await soundObject2.pauseAsync();
		await soundObject3.pauseAsync();
		await soundObject4.pauseAsync();
			} catch (error) {
		console.log(error);
	}
	
}
